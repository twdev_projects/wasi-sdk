let wasm_memory = [];
let args = [ "hw", "some", "silly", "fake", "args" ];

function args_get(argv_pp, argv_buf_p) {
  console.log("args_get args:", arguments);
  let m = new DataView(wasm_memory);
  let a = new Uint8Array(wasm_memory);
  const ptr_size = 4;

  for (arg of args) {
    m.setUint32(argv_pp, argv_buf_p, true);
    argv_pp += ptr_size;

    const argEncd = new TextEncoder().encode(arg);
    a.set(argEncd, argv_buf_p);
    // append zero terminator
    m.setUint8(argv_buf_p + arg.length, 0);

    argv_buf_p += arg.length + 1;
  }
}

function args_sizes_get(args_p, args_len_p) {
  console.log("args_sizes_get args:", arguments);
  let m = new DataView(wasm_memory);
  m.setUint32(args_p, args.length, true);

  let args_total_len = 0;
  for (arg of args) {
    args_total_len += arg.length + 1;
  }
  m.setUint32(args_len_p, args_total_len, true);
}

function environ_get() { console.log("environ_get args:", arguments); }

function environ_sizes_get() {
  console.log("environ_sizes_get args:", arguments);
}

function fd_close() { console.log("fd_close args:", arguments); }

function fd_fdstat_get(fd, fdstat_p) {
  console.log("fd_fdstat_get args:", arguments);
  if (fd < 1 || fd > 2) {
    throw "Unsupported file descriptor";
  }

  let m = new DataView(wasm_memory);

  const filetype_character_device = 2;
  const filetype_offs = 0;
  const filetype = filetype_character_device;
  m.setUint16(filetype_offs, filetype, true);

  const fdflags = 0;
  const fdflags_offs = 2;
  m.setUint16(fdflags_offs, fdflags, true);

  const rights = 0x28;
  const rights_offs = 8;
  m.setUint32(rights_offs, rights, true);

  const rights_inh = 0x28;
  const rights_inh_offs = 16;
  m.setUint32(rights_inh_offs, rights_inh, true);
}

function fd_read() { console.log("fd_read args:", arguments); }

function fd_seek() { console.log("fd_seek args:", arguments); }

function fd_write(fd, ciovec_arr_p, ciovec_arr_len, size_p) {
  console.log("fd_write args:", arguments);
  if (fd < 1 || fd > 2) {
    throw "Unsupported file descriptor";
  }
  let m = new DataView(wasm_memory);

  let s = 0;
  let str = "";

  for (let i = 0; i < ciovec_arr_len; i++) {
    let buf_p = m.getUint32(ciovec_arr_p, true);
    ciovec_arr_p += 4;

    let buf_len = m.getUint32(ciovec_arr_p, true);
    ciovec_arr_p += 4;

    let sv = new DataView(wasm_memory, buf_p, buf_len);
    let d = new TextDecoder().decode(sv);

    str += d;
    s += buf_len;
  }

  if (str.length > 0) {
    if (fd == 1) {
      console.log(str);
    } else {
      console.error(str);
    }
  }

  m.setUint32(size_p, s, true);
}

function proc_exit() { console.log("proc_exit args:", arguments); }

const importObject = {
  wasi_snapshot_preview1 : {
    args_get : args_get,
    args_sizes_get : args_sizes_get,

    environ_get : environ_get,
    environ_sizes_get : environ_sizes_get,

    fd_close : fd_close,
    fd_fdstat_get : fd_fdstat_get,
    fd_read : fd_read,
    fd_seek : fd_seek,
    fd_write : fd_write,

    proc_exit,
  },
};

WebAssembly.instantiateStreaming(fetch("hw.wasm"), importObject).then((obj) => {
  let wasm_exp = obj.instance.exports;
  wasm_memory = obj.instance.exports.memory.buffer;

  wasm_exp._start();
});

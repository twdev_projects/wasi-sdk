# wasi-sdk

Example on how to use [WASI-SDK](https://github.com/WebAssembly/wasi-sdk) and
how to implement WASI polyfills for the browser.

Read more on [my blog](https://twdev.blog/2023/11/wasm_cpp_04/).

## Building

```bash
source wasmenv.sh

./build.sh
```

### Prerequisites

You need [WASI-SDK](https://github.com/WebAssembly/wasi-sdk) installed under
/opt.  `wasmenv.sh` assumes version 20 used - this might have to be adjusted if
your version differs.
